from helpers.API import ApiPetHelper
import allure
from helpers.testdata import ApiPetNames


def test_add_new_pet():
    api_client = ApiPetHelper()

    with allure.step("adding new pet"):
        new_pet = api_client.add_new_pet(ApiPetNames.PET_NAME_1)
        pet_details = api_client.get_pet_by_id(new_pet['id'])

    with allure.step("checking correct name is returned in GET"):
        assert pet_details['name'] == ApiPetNames.PET_NAME_1,\
            "names don't match"


def test_update_pet_name():
    api_client = ApiPetHelper()

    with allure.step("adding and updating pet name"):
        new_pet = api_client.add_new_pet(ApiPetNames.PET_NAME_2)
        api_client.update_pet(new_pet['id'], 'name',
                              ApiPetNames.UPDATED_PET_NAME_2)

    with allure.step("checking updated name is returned in GET"):
        pet_details = api_client.get_pet_by_id(new_pet['id'])
        assert pet_details['name'] == ApiPetNames.UPDATED_PET_NAME_2
