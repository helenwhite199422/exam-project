import pytest
from selenium import webdriver
from helpers.DB import DataBaseHelper
from helpers.testdata import Data, DataBaseTables


@pytest.fixture(scope="module")
def connect_db():
    db = DataBaseHelper()
    yield db
    db.close_connection()


@pytest.fixture(scope="module")
def driver():
    driver = webdriver.Chrome()
    yield driver
    driver.quit()


@pytest.fixture(scope="module")
def clear_data(connect_db):
    yield
    connect_db.delete(DataBaseTables.TABLE_WITH_GROUPS,
                      f"name = '{Data.GROUP}'")
    connect_db.delete(DataBaseTables.TABLE_WITH_USERS,
                      f"username = '{Data.USERNAME2}'")
