import allure
from helpers.API import ApiUserHelper
from helpers.testdata import ApiUserData as Api


def test_create_new_user():
    api_client = ApiUserHelper()

    with allure.step("creating a new user "):
        new_user = api_client.create_new_user(Api.USER_ID, Api.USERNAME,
                                              Api.PASSWORD)

    with allure.step("checking that response status is 200 OK"):
        assert new_user['code'] == 200, "user is not created"


def test_user_login():
    api_client = ApiUserHelper()

    with allure.step("log in as user"):
        login = api_client.login(Api.USERNAME, Api.PASSWORD)

    with allure.step("checking that message from response contains"):
        assert "logged in user session" in login['message']


def test_get_user_details():
    api_client = ApiUserHelper()

    with allure.step("getting user details"):
        get_details = api_client.get_user_details(Api.USERNAME)

    with allure.step("checking user details"):
        assert get_details['username'] == Api.USERNAME and \
               get_details['id'] == int(Api.USER_ID)


def test_user_logout():
    api_client = ApiUserHelper()
    logout = api_client.logout()

    with allure.step("checking successful response"):
        assert logout['code'] == 200, "user is not logged out"


def test_delete_user():
    api_client = ApiUserHelper()
    with allure.step("deleting user"):
        api_client.delete_user(Api.USERNAME)
    get_details = api_client.get_user_details(Api.USERNAME)

    with allure.step("checking that deleted user is not found"):
        assert get_details['message'] == 'User not found'
