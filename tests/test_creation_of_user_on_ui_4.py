from pages.main_page import MainPage
from pages.dashboard_page import DashboardPage
import allure
from helpers.testdata import DataBaseTables as Tables
from helpers.testdata import Data
from helpers.testdata import AdminCredentials as Admin


def test_login_by_admin(driver):

    with allure.step("log in as admin user"):
        page = MainPage(driver)
        page.open_page()
        login_page = page.open_login_page()
        dashboard_page = login_page.login(Admin.USERNAME_ADMIN,
                                          Admin.PASSWORD_ADMIN)

    with allure.step("checking correct username in WELCOME header"):
        assert dashboard_page.correct_user_logged() ==\
               Admin.USERNAME_ADMIN.upper(), "Not admin is logged"


def test_create_new_user_from_ui(driver):
    dashboard_page = DashboardPage(driver)
    with allure.step("creating new user"):
        create_user_page = dashboard_page.open_create_user_page()
        create_user_page.create_user(Data.USERNAME2, Data.PASSWORD2)

    with allure.step("checking create notification message"):
        assert create_user_page.get_successful_create_message() ==\
               Data.USERNAME2


def test_created_user_in_db(connect_db, clear_data):
    with allure.step("checking created user is present in DB"):
        new_group = connect_db.select(Tables.TABLE_WITH_USERS,
                                      f"username = '{Data.USERNAME2}'")

    assert new_group[0][4] == Data.USERNAME2


def test_logout(driver):
    dashboard_page = DashboardPage(driver)
    dashboard_page.log_out()

    with allure.step("checking that user is redirected to logout page"):
        assert driver.current_url == 'http://localhost:8000/admin/logout/', \
            "Not Logout Page has been opened"


def test_login_as_new_user(driver):
    page = MainPage(driver)
    with allure.step("log in as new user"):
        page.open_page()
        login_page = page.open_login_page()
        dashboard_page = login_page.login(Data.USERNAME2, Data.PASSWORD2)

    assert dashboard_page.correct_user_logged() == Data.USERNAME2.upper()
