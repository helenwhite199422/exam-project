from pages.main_page import MainPage
from pages.dashboard_page import DashboardPage
from pages.edit_user_page import EditUserPage
import allure
from helpers.testdata import DataBaseTables as Tables
from helpers.testdata import Data
from helpers.testdata import AdminCredentials as Admin


def test_create_group(connect_db):
    with allure.step("creating a new group in DB "):
        connect_db.insert(Tables.TABLE_WITH_GROUPS, '(name)',
                          f"('{Data.GROUP1}')")
    with allure.step("getting created group details"):
        new_group = connect_db.select(Tables.TABLE_WITH_GROUPS,
                                      f"name ='{Data.GROUP1}'")

    assert new_group[0][1] == Data.GROUP1


def test_login_by_admin(driver):

    with allure.step("log in as admin user"):
        page = MainPage(driver)
        page.open_page()
        login_page = page.open_login_page()
        dashboard_page = login_page.login(Admin.USERNAME_ADMIN,
                                          Admin.PASSWORD_ADMIN)

    with allure.step("checking correct username in WELCOME header"):
        assert dashboard_page.correct_user_logged() == \
               Admin.USERNAME_ADMIN.upper(), "Not admin is logged"


def test_create_new_user_from_ui(driver):
    dashboard_page = DashboardPage(driver)

    with allure.step("creating new user"):
        create_user_page = dashboard_page.open_create_user_page()
        create_user_page.create_user(Data.USERNAME1, Data.PASSWORD1)

    with allure.step("checking create notification message"):
        assert create_user_page.get_successful_create_message() == \
               Data.USERNAME1


def test_add_user_to_group(driver):
    edit_user = EditUserPage(driver)
    with allure.step("adding user to group"):
        edit_user.open_user(Data.USERNAME1)
        edit_user.add_user_to_group(Data.GROUP1)

    with allure.step("checking update notification message"):
        assert "was changed successfully." in \
               edit_user.get_successful_update_message()


def test_check_user_added_to_group_in_bd(connect_db):
    with allure.step("getting ids of user and group"):
        new_user_id = (connect_db.select(Tables.TABLE_WITH_USERS,
                                    f"username = '{Data.USERNAME1}'"))[0][0]
        new_group_id = (connect_db.select(Tables.TABLE_WITH_GROUPS,
                                          f"name = '{Data.GROUP1}'"))[0][0]

    with allure.step("checking the record with user and group is present"):
        users_in_groups = connect_db.select(
            Tables.TABLE_WITH_RELATED_USER_AND_GROUP,
            f"user_id = '{new_user_id}' AND"
            f" group_id = '{new_group_id}'")
        assert users_in_groups, "/Such group or user doesn't exist"
