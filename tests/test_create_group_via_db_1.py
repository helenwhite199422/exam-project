from pages.main_page import MainPage
from pages.dashboard_page import DashboardPage
import allure
from helpers.testdata import AdminCredentials as Admin
from helpers.testdata import DataBaseTables as Tables
from helpers.testdata import Data


def test_create_group(connect_db, clear_data):
    with allure.step("creating a new group in DB "):
        connect_db.insert(Tables.TABLE_WITH_GROUPS, '(name)',
                          f"('{Data.GROUP}')")
    with allure.step("getting created group details"):
        new_group = connect_db.select(Tables.TABLE_WITH_GROUPS,
                                      f"name ='{Data.GROUP}'")

    assert new_group[0][1] == Data.GROUP


def test_login_by_admin(driver):
    with allure.step("log in as admin user"):
        page = MainPage(driver)
        page.open_page()
        login_page = page.open_login_page()
        dashboard_page = login_page.login(Admin.USERNAME_ADMIN,
                                          Admin.PASSWORD_ADMIN)

    with allure.step("checking correct username in header"):
        assert dashboard_page.correct_user_logged() == \
               Admin.USERNAME_ADMIN.upper(), "Not admin is logged"


def test_check_created_group_on_ui(driver):
    with allure.step("opening the page with groups"):
        dashboard_page = DashboardPage(driver)
        dashboard_page.open_groups_page()

    with allure.step("checking group is present in list of existing groups"):
        assert Data.GROUP in dashboard_page.get_existing_groups()
