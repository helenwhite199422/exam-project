from pages.main_page import MainPage
from pages.dashboard_page import DashboardPage
import allure
from helpers.testdata import AdminCredentials as Admin


def test_login_by_admin(driver):

    page = MainPage(driver)
    with allure.step("log in as admin user"):
        page.open_page()
        login_page = page.open_login_page()
        dashboard_page = login_page.login(Admin.USERNAME_ADMIN,
                                          Admin.PASSWORD_ADMIN)

    with allure.step("checking correct username in WELCOME header"):
        assert dashboard_page.correct_user_logged() == \
               Admin.USERNAME_ADMIN.upper(), "Not admin is logged"


def test_delete_first_element(driver):
    dashboard_page = DashboardPage(driver)
    with allure.step("opening 1st created image"):
        change_post_page = dashboard_page.open_first_post()
    with allure.step("getting src of the image"):
        deleted_src = change_post_page.get_deleted_image_src()
        with allure.step("deleting image"):
            change_post_page.delete_post()
    with allure.step("opening main page"):
        main_page = MainPage(driver)
        main_page.open_page()

    with allure.step("checking that 1st image src != deleted src "):
        assert deleted_src != main_page.get_first_image_src()
