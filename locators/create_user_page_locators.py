from selenium.webdriver.common.by import By


class CreateUserPageLocators:

    USERNAME_INPUT = (By.ID, 'id_username')
    PASSWORD_INPUT = (By.ID, 'id_password1')
    CONFIRM_PASSWORD_INPUT = (By.ID, 'id_password2')

    SUCCESSFUL_CREATION = (By.XPATH, '//li[@class="success"]/descendant::a')

    SUPERUSER = (By.ID, 'id_is_staff')

    SAVE_AND_CONTINUE = (By.XPATH, '//input[@name="_continue"]')
    SAVE_BUTTON = (By.XPATH, '//input[@name="_save"]')
