from selenium.webdriver.common.by import By


class EditUserPageLocators:
    USERS = (By.XPATH, '//th[@class="field-username"]/descendant::a')

    GROUPS = (By.XPATH, '//select[@name="groups_old"]/descendant::option')
    SELECT_GROUP = (By.ID, 'id_groups_add_link')

    SAVE_BUTTON = (By.XPATH, '//input[@name="_save"]')

    SUCCESSFUL_UPDATE = (By.XPATH, '//li[@class="success"]')
