from selenium.webdriver.common.by import By


class LoginPageLocators:
    ADMIN_FORM = (By.ID, "container")
    USERNAME = (By.XPATH, '//input[@name="username"]')
    PASSWORD = (By.ID, 'id_password')
    LOGIN_BUTTON = (By.XPATH, '//input[@value="Log in"]')
