from selenium.webdriver.common.by import By


class DashboardLocators:
    LOGGED_USER = (By.XPATH, '//strong')
    ADD_USER_BUTTON = (By.XPATH,
                '//tr[@class="model-user"]/descendant::a[@class="addlink"]')
    POSTS_LINK = (By.XPATH, '//a[text()="Posts"]')
    POSTS = (By.XPATH, '//a[contains(@href,"/change")]')
    GROUPS = (By.XPATH, '//a[text()="Groups"]')

    EXISTING_GROUPS = (By.XPATH, '//table[@id="result_list"]//descendant::a')

    LOGOUT_BUTTON = (By.XPATH, '//a[text()="Log out"]')
