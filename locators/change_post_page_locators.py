from selenium.webdriver.common.by import By


class ChangePostPageLocators:
    DELETE_BUTTON = (By.XPATH, '//a[@class="deletelink"]')
    IMAGE_LINK = (By.XPATH, '//textarea')
    VIEW_SITE = (By.XPATH, '//a[text()="View site"]')


class ChangePostConfirmationPageLocators:
    YES_BUTTON = (By.XPATH, '//input[@type="submit"]')
