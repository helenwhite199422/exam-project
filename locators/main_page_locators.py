from selenium.webdriver.common.by import By


class MainPageLocators:
    GO_TO_ADMIN_BUTTON = (By.XPATH, '//a[text()="Go to Admin"]')
    IMAGES = (By.XPATH, '//img[@class="card-img-top"]')
