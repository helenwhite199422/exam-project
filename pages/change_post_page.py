from pages.base_page import BasePage
from locators.change_post_page_locators import ChangePostPageLocators
from locators.change_post_page_locators import\
    ChangePostConfirmationPageLocators


class ChangePostPage(BasePage):

    def __init__(self, driver, url):
        super().__init__(driver, url)

    def delete_post(self):
        self.driver.find_element(*ChangePostPageLocators.DELETE_BUTTON).click()
        self.driver.find_element(*ChangePostConfirmationPageLocators.
                                 YES_BUTTON).click()

    def get_deleted_image_src(self):
        src = self.driver.find_element(*ChangePostPageLocators.IMAGE_LINK).text
        return 'data:image/png;base64,/' + src

    def open_admin_page(self):
        self.driver.find_element(*ChangePostPageLocators.VIEW_SITE).click()
