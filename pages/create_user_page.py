from pages.base_page import BasePage
from locators.create_user_page_locators import CreateUserPageLocators


class CreateUserPage(BasePage):

    def __init__(self, driver, url):
        super().__init__(driver, url)

    def create_user(self, username, password):
        self.driver.find_element(*CreateUserPageLocators.USERNAME_INPUT).\
            send_keys(username)
        self.driver.find_element(*CreateUserPageLocators.PASSWORD_INPUT).\
            send_keys(password)
        self.driver.find_element(*CreateUserPageLocators.
                                 CONFIRM_PASSWORD_INPUT).send_keys(password)
        self.driver.find_element(*CreateUserPageLocators.
                                 SAVE_AND_CONTINUE).click()
        self.driver.find_element(*CreateUserPageLocators.SUPERUSER).click()
        self.driver.find_element(*CreateUserPageLocators.SAVE_BUTTON).click()

    def get_successful_create_message(self):
        return self.driver.find_element(*CreateUserPageLocators.
                                        SUCCESSFUL_CREATION).text
