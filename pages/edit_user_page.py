from pages.base_page import BasePage
from locators.edit_user_page_locators import EditUserPageLocators
import time


class EditUserPage(BasePage):
    URL = "http://localhost:8000/admin/auth/user/"

    def __init__(self,driver):
        super().__init__(driver, self.URL)

    def open_user(self, username):
        users = self.driver.find_elements(*EditUserPageLocators.USERS)
        for i in users:
            if i.text == username:
                i.click()

    def add_user_to_group(self, group):
        existing_groups = self.driver.find_elements(*EditUserPageLocators.
                                                    GROUPS)
        for i in existing_groups:
            if i.get_attribute('title') == group:
                i.click()
        self.driver.find_element(*EditUserPageLocators.SELECT_GROUP).click()
        time.sleep(6)
        self.driver.find_element(*EditUserPageLocators.SAVE_BUTTON).click()
        time.sleep(6)

    def get_successful_update_message(self):
        return self.driver.find_element(
            *EditUserPageLocators.SUCCESSFUL_UPDATE).text
