from pages.base_page import BasePage
from locators.login_page_locators import LoginPageLocators
from pages.dashboard_page import DashboardPage


class LoginPage(BasePage):

    def __init__(self, driver, url):
        super().__init__(driver, url)

    def login(self, username, password):
        username_element = self.driver.find_element(*LoginPageLocators.
                                                    USERNAME)
        username_element.send_keys(username)
        password_element = self.driver.find_element(*LoginPageLocators.
                                                    PASSWORD)
        password_element.send_keys(password)
        login_button = self.driver.find_element(*LoginPageLocators.
                                                LOGIN_BUTTON)
        login_button.click()
        return DashboardPage(self.driver)
