from pages.base_page import BasePage
from locators.main_page_locators import MainPageLocators
from pages.login_page import LoginPage


class MainPage(BasePage):
    URL = "http://localhost:8000/"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def open_login_page(self):
        login_page = self.driver.find_element(*MainPageLocators.
                                              GO_TO_ADMIN_BUTTON)
        login_page.click()
        return LoginPage(self.driver, self.driver.current_url)

    def get_first_image_src(self):
        all_images = self.driver.find_elements(*MainPageLocators.IMAGES)
        for i in all_images:
            src_list = i.get_attribute('src')
        return src_list
