from pages.base_page import BasePage
from locators.dashboard_page_locators import DashboardLocators
from pages.create_user_page import CreateUserPage
from pages.change_post_page import ChangePostPage


class DashboardPage(BasePage):
    URL = "http://localhost:8000/admin/"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def correct_user_logged(self):
        welcome_name = self.driver.find_element(*DashboardLocators.LOGGED_USER)
        result = welcome_name.text
        return result

    def open_create_user_page(self):
        item = self.driver.find_element(*DashboardLocators.ADD_USER_BUTTON)
        item.click()
        return CreateUserPage(self.driver, self.driver.current_url)

    def log_out(self):
        logout = self.driver.find_element(*DashboardLocators.LOGOUT_BUTTON)
        logout.click()

    def open_first_post(self):
        self.driver.find_element(*DashboardLocators.POSTS_LINK).click()
        all_posts = self.driver.find_elements(*DashboardLocators.POSTS)
        for i in all_posts:
            first_post = i
        first_post.click()
        return ChangePostPage(self.driver, self.driver.current_url)

    def open_groups_page(self):
        self.driver.find_element(*DashboardLocators.GROUPS).click()

    def get_existing_groups(self):
        res = []
        groups = self.driver.find_elements(*DashboardLocators.EXISTING_GROUPS)
        for i in groups:
            res.append(i.text)
        return res
