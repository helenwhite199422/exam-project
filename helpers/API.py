import requests


def check_response(response):
    response.raise_for_status()
    return response


class BaseApiHelper:

    def __init__(self):
        self.URL = "https://petstore.swagger.io/"


class ApiPetHelper(BaseApiHelper):

    def __init__(self):
        super().__init__()
        self.url = self.URL + 'v2/pet/'

    def get_pet_by_id(self, pet_id):
        return check_response(requests.get(self.url + str(pet_id))).json()

    def add_new_pet(self, pet_name):
        data = {
            "name": pet_name
        }
        return check_response(requests.post(self.url, json=data)).json()

    def update_pet(self, pet_id, updated_entity, new_value):
        data = {
            "id": pet_id,
            updated_entity: new_value

        }
        return check_response(requests.post(self.url, json=data)).json()


class ApiUserHelper(BaseApiHelper):

    def __init__(self):
        super().__init__()
        self.url = self.URL + 'v2/user/'

    def get_user_details(self, username):
        return requests.get(self.url + username).json()

    def create_new_user(self, user_id, username, password):
        data = {
            "id": user_id,
            "username": username,
            "password": password
        }
        return check_response(requests.post(self.url, json=data)).json()

    def login(self, username, password):
        parameters = {'username': username, "password": password}
        return check_response(requests.get(self.url + 'login/',
                                           params=parameters)).json()

    def logout(self):
        return check_response(requests.get(self.url + 'logout')).json()

    def delete_user(self, username):
        return check_response(requests.delete(self.url + username)).json()
