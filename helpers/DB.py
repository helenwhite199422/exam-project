import psycopg2
from helpers.testdata import DataBaseCredentials


class DataBaseHelper:

    def __init__(self):
        self.dbname = DataBaseCredentials.DBNAME
        self.user = DataBaseCredentials.USER
        self.password = DataBaseCredentials.PASSWORD
        self.host = DataBaseCredentials.HOST
        self.db_connection = psycopg2.connect(dbname=self.dbname,
                                              user=self.user,
                                              password=self.password,
                                              host=self.host)
        self.cursor = self.db_connection.cursor()

    def select(self, table, conditions):
        self.cursor.execute(f"SELECT * FROM {table} WHERE {conditions}")
        return self.cursor.fetchall()

    def insert(self, table, columns, values):
        self.cursor.execute(f"INSERT INTO {table} {columns} VALUES {values}")
        self.db_connection.commit()

    def delete(self, table, conditions):
        self.cursor.execute(f"DELETE FROM {table} WHERE {conditions}")
        self.db_connection.commit()

    def close_connection(self):
        self.db_connection.close()
