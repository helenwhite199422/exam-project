class DataBaseCredentials:

    DBNAME = 'postgres'
    USER = 'postgres'
    PASSWORD = 'postgres'
    HOST = 'localhost'


class ApiPetNames:

    PET_NAME_1 = "Benny"
    PET_NAME_2 = "Rose"
    UPDATED_PET_NAME_2 = "Rose_upd"


class ApiUserData:
    USER_ID = '121212'
    USERNAME = 'alextest01'
    PASSWORD = 'qwerty123'


class AdminCredentials:
    USERNAME_ADMIN = 'admin'
    PASSWORD_ADMIN = 'password'


class DataBaseTables:
    TABLE_WITH_GROUPS = 'auth_group'
    TABLE_WITH_USERS = 'auth_user'
    TABLE_WITH_RELATED_USER_AND_GROUP = 'auth_user_groups'


class Data:
    GROUP = 'GroupTest'

    USERNAME1 = 'testuser1'
    PASSWORD1 = 'HelloNewComer123'
    GROUP1 = 'GroupTest1'

    USERNAME2 = 'testuser2'
    PASSWORD2 = 'HelloNewComer123'
